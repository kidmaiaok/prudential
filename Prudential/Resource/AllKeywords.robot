*** Settings ***
Library           Selenium2Library
Library           CustomXlsxLibrary
Library           String
Library           Collections
Library           OperatingSystem
Library           CustomCollections
Library           Process
Library           DateTime
Resource          PageKeywords/SeleniumRedefineKeyword.robot
Resource          AllRepository.robot
Resource          AllVariables.robot
Resource          PageKeywords/PublicKeywords.robot
Resource          PageKeywords/MaxsavingKeywords.robot
Library           AutoItLibrary
