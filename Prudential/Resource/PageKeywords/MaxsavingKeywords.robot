*** Settings ***
Resource          ../AllKeywords.robot

*** Keywords ***
Input Insurance Form
    Click Web Element    ${OIC_datepicker_BirthDate}
    # fix birthdate for today
    Input Web Text    ${PayorDetails_textbox_BirthDate}    14-01-1960
    Click Web Element    //div[@aria-selected='true']
    Comment    sleep    3
    Comment    Comment    Selenium2Library.Press Key    ${PayorDetails_textbox_BirthDate}    \\13
    Comment    Comment    Click Web Element    //h3[contains(.,'Personal information')] | //h3[contains(.,'Personal information')]
    Comment    Click Web Element    ${OIC_list_MarriageStatus}
    Comment    sleep    2
    Select Listbox From Text    ${OIC_list_MarriageStatus}    ${MarriageStatusSingle}
    Select Listbox From Text    ${OIC_list_NumberOfChildren}    ${NumberOfChildrenNon}
    Select Listbox From Text    ${OIC_list_Education}    ${EducationLowerThanBachelorDegree}
    Select Listbox From Text    ${OIC_list_Occupation}    ${OccupationBusinessOwner}
    Select Listbox From Text    ${OIC_list_DailyLifeNecessary}    ${DoYouNeedThisMoneyForDailyLifeNotNecessary}
    Click Web Element    ${OIC_button_FinicialInformation_No}
    Click Web Element    ${OIC_chkbox_ForFamily}

Select Listbox From Text
    [Arguments]    ${locator}    ${text}
    ${xpath_listbox_value}    Set Variable    xpath=//ul[@role='listbox']/li[text()='${text}']
    Click Web Element    ${locator}
    @{list_element}    Get WebElements    ${xpath_listbox_value}
    ${count_element}    Selenium2Library.Get Element Count    ${xpath_listbox_value}
    ${count_element}    Evaluate    ${count_element}-1
    Click Web Element    @{list_element}[${count_element}]
    Comment    Comment    Click Web Element    ${xpath_listbox_value}
    Comment    :FOR    ${ELEMENT}    IN    @{list_element}
    Comment    \    ${element_status}    BuiltIn.Run Keyword And Return Status    Element Should Be Visible    ${ELEMENT}
    Comment    \    Run Keyword If    ${element_status} == ${True}    Click Element    ${ELEMENT}
    ...    ELSE    Exit For Loop
    Comment    \    Selenium2Library.Set Selenium Speed

Input Personal Information
    Select Listbox From Text    ${PersonalInformation_list_Title}    ${TitleMiss}
    Input Web Text    ${PersonalInformation_textbox_GivenName}    Devid
    Input Web Text    ${PersonalInformation_textbox_FamilyName}    Backham
    Click Web Element    ${PersonalInformation_button_Gender_Female}
    Click Web Element    ${PersonalInformation_textbox_Occupation}
    Click Web Element    //div[@alt='1299-']
    Input Web Text    ${PersonalInformation_textbox_AnnualIncome}    500000
    Input Web Text    ${PersonalInformation_textbox_AssestsUnderManagement}    0
    Select Listbox From Text    ${PersonalInformation_textbox_TaxRate}    30%
    Input Web Text    ${PersonalInformation_textbox_EmailAddress}    test@gmail.com

Verify Benefit Illustration Page
    Web Element Should Be Visible    ${BenefitIllustration_button_Done}
    Web Element Should Be Visible    ${BenefitIllustration_button_Email}
    Web Element Should Be Visible    ${BenefitIllustration_button_Proposal}

Verify Payor Detail Form
    Web Element Should Be Visible    ${PayorDetails_list_Title}
    Web Element Should Be Visible    ${PayorDetails_textbox_GivenName}
    Web Element Should Be Visible    ${PayorDetails_textbox_FamilyName}
    Web Element Should Be Visible    ${PayorDetails_button_Gender_Male}
    Web Element Should Be Visible    ${PayorDetails_button_Gender_Female}
    Web Element Should Be Visible    ${PayorDetails_datepicker_BirthDate}
    Web Element Should Be Visible    ${PayorDetails_textbox_Occupation}
    Web Element Should Be Visible    ${PayorDetails_textbox_EmailAddress}
    Web Element Should Be Visible    ${PayorDetails_button_Next}

Verify Personal Information Form
    Web Element Should Be Visible    ${PersonalInformation_list_Title}
    Web Element Should Be Visible    ${PersonalInformation_textbox_GivenName}
    Web Element Should Be Visible    ${PersonalInformation_textbox_FamilyName}
    Web Element Should Be Visible    ${PersonalInformation_button_Gender_Male}
    Web Element Should Be Visible    ${PersonalInformation_button_Gender_Female}
    Web Element Should Be Visible    ${PersonalInformation_textbox_DateOfBirth}
    Web Element Should Be Visible    ${PersonalInformation_textbox_Occupation}
    Web Element Should Be Visible    ${PersonalInformation_textbox_AnnualIncome}
    Web Element Should Be Visible    ${PersonalInformation_textbox_AssestsUnderManagement}
    Web Element Should Be Visible    ${PersonalInformation_textbox_TaxRate}
    Web Element Should Be Visible    ${PersonalInformation_textbox_EmailAddress}

Verify Insurance Form
    Web Element Should Be Visible    ${OIC_datepicker_BirthDate}
    Web Element Should Be Visible    ${OIC_list_MarriageStatus}
    Web Element Should Be Visible    ${OIC_list_NumberOfChildren}
    Web Element Should Be Visible    ${OIC_list_Education}
    Web Element Should Be Visible    ${OIC_list_Occupation}
    Web Element Should Be Visible    ${OIC_list_DailyLifeNecessary}
    Web Element Should Be Visible    ${OIC_button_FinicialInformation_No}
    Web Element Should Be Visible    ${OIC_button_FinicialInformation_Yes}
    Web Element Should Be Visible    ${OIC_chkbox_ForFamily}
    Web Element Should Be Visible    ${OIC_chkbox_ForRetirementPlan}
    Web Element Should Be Visible    ${OIC_chkbox_ForSaving}
    Web Element Should Be Visible    ${OIC_chkbox_ForTaxBenefit}
    Web Element Should Be Visible    ${OIC_chkbox_other}
    Web Element Should Be Visible    ${OIC_button_Next}

Input Payor Details
    Comment    Select Listbox From Text    ${PayorDetails_list_Title}    ${TitleMiss}
    Comment    Input Web Text    ${PayorDetails_textbox_GivenName}    Devid
    Comment    Input Web Text    ${PayorDetails_textbox_FamilyName}    Backham
    Comment    Click Web Element    ${PayorDetails_datepicker_BirthDate}
    sleep    5

Input Insurance Plan
    [Arguments]    ${SumAssured}
    Click Web Element    ${InsurancePlan_button_AddMainPlan}
    Click Web Element    ${InsurancePlan_chkbox_PRUMaxSaving}
    Click Web Element    ${InsurancePlan_button_Ok}
    Input Web Text    ${InsurancePlan_textbox_SumAssured}    ${SumAssured}
    Click Web Element    ${InsurancePlan_button_Next}

Input Benefit Illustration
    Click Web Element    ${BenefitIllustration_button_Proposal}    #proposal

Input Personal Additional Info
    Select Listbox From Text    ${PersonalAdditionalInfo_list_Nationality}    Thai
    Select Listbox From Text    ${PersonalAdditionalInfo_list_CountryOfBirth}    Thailand
    Select Listbox From Text    ${PersonalAdditionalInfo_list_IdType}    ID Card
    Input Web Text    ${PersonalAdditionalInfo_textbox_IDNumber}    1103701488701
    #PersonalAddtionalInfo
    Select Listbox From Text    ${PersonalAdditionalInfo_list_Nationality}    Thai
    Select Listbox From Text    ${PersonalAdditionalInfo_list_CountryOfBirth}    Thailand
    Select Listbox From Text    ${PersonalAdditionalInfo_list_IdType}    ID Card
    Input Web Text    ${PersonalAdditionalInfo_textbox_IDNumber}    1103701488701
    #DateExpire
    Click Web Element    ${PersonalAdditionalInfo_datepicker_ExpiryDate}
    Input Web Text    ${PersonalAdditionalInfo_textbox_ExpiryDate}    15-02-2019
    Click Web Element    ${PersonalAdditionalInfo_label_ExpiryDate}
    Select Listbox From Text    ${PersonalAdditionalInfo_list_MaritalStatus}    Single
    Click Web Element    ${PersonalAdditionalInfo_button_ InsuranceForYourself_No}    #No
    Input Web Text    ${PersonalAdditionalInfo_textbox_CurrentAddressNumber}    124
    Run Keywords    Click Web Element    ${PersonalAdditionalInfo_list_CurrentAddressArea}
    ...    AND    Click Web Element    xpath=//li[@title='Bangkok']
    ...    AND    Click Web Element    xpath=//li[@title='Khlong Toei']
    ...    AND    Click Web Element    xpath=//li[@title='Khlong Tan']
    ...    AND    Click Web Element    xpath=//li[@title='10110']
    Input Web Text    ${PersonalAdditionalInfo_textbox_CurrentAddressMobile}    0851169494
    Select Listbox From Text    ${PersonalAdditionalInfo_list_WorkingAddress}    Same as Current Address
    Select Listbox From Text    ${PersonalAdditionalInfo_list_HouseRegisterAddress}    Same as Current Address
    Select Listbox From Text    ${PersonalAdditionalInfo_list_PolicyMailingAddress}    Current Address
    Select Listbox From Text    ${PersonalAdditionalInfo_list_MailingAddress}    Current Address
    Input Web Text    ${PersonalAdditionalInfo_textbox_Duty}    Tester

Input Payor Additional Info
    Click Web Element    ${PayorAdditionalInfo_button_IsPaySelf}    #Yes

Input Beneficiary Additional Info
    sleep    4
    Click Web Element    ${BeneficiaryInfo_button_AddBeneficiary}
    sleep    2
    Select Listbox From Text    ${BeneficiaryInfo_list_Relationship}    Wife
    Select Listbox From Text    ${BeneficiaryInfo_list_Title}    MISS
    Input Web Text    ${BeneficiaryInfo_textbox_GivenName}    Hello
    Input Web Text    ${BeneficiaryInfo_textbox_FamilyName}    World
    Input Web Text    ${BeneficiaryInfo_textbox_Age}    34
    Input Web Text    ${BeneficiaryInfo_textbox_Ratio}    100
    Selenium2Library.Scroll Element Into View    ${BeneficiaryInfo_list_Address}
    Select Listbox From Text    ${BeneficiaryInfo_list_Address}    Other
    Click Web Element    ${BeneficiaryInfo_button_Ok}
    Capture Page Screenshot
    sleep    2

Input Lifestyle Questions
    Click Web Element    ${LifestyleQuestions_button_QuestionOne_Not}
    Click Web Element    ${LifestyleQuestions_button_QuestionTwo_Never}
    Click Web Element    ${LifestyleQuestions_button_QuestionThree_Never}
    Click Web Element    ${LifestyleQuestions_button_QuestionFour_HaveNeverUsed}
    Click Web Element    ${LifestyleQuestions_button_QuestionFive_Never}
    Click Web Element    ${LifestyleQuestions_button_QuestionSix_DoNotSmoke}
    Click Web Element    ${LifestyleQuestions_button_QuestionSeven_No}
    Input Web Text    ${LifestyleQuestions_textbox_InsuredHeight}    170
    Input Web Text    ${LifestyleQuestions_textbox_InsuredWeight}    70
    Click Web Element    ${LifestyleQuestions_button_WeightChange_Stationary}

Input FATCA
    Click Web Element    ${FATCA_button_ConfirmationOnStatusA_No}
    Click Web Element    ${FATCA_button_ConfirmationOnStatusB_No}
    Click Web Element    ${FATCA_button_TaxNotice_No}

Input KYC/CDD Question
    Click Web Element    ${CDDQuestion_button_PEPs_No}
    Select Listbox From Text    ${CDDQuestion_list_ObjectiveTransaction}    Life Assurance
    Select Listbox From Text    ${CDDQuestion_list_MajorOccupation}    Company Employee
    Select Listbox From Text    ${CDDQuestion_list_CurrentAnnualIncome}    Less than 500,000
    Click Web Element    ${CDDQuestion_button_OtherIncome_No}

Input Submission UOB Information
    Select Listbox From Text    ${UOBInformation_list_BranchCode}    001/Si Phraya Branch
    Click Web Element    ${UOBInformation_button_NoCampaign}

Input Submission Payment
    Select Listbox From Text    ${Payment_list_PaymentMethod}    Cash
    Select Listbox From Text    ${Payment_list_DividendOption}    Paid for Premium

Input Submission Documents
    sleep    2
    Click Web Element    (//button[@class='ant-btn viewBtn___CGYTh']/span[text()='Browse'])[1]
    sleep    2
    Upload File    (//div[@class='uploadBtn___1BT3c'])[1]    ThaiID.jpg
    Click Web Element    (//button[@class='ant-btn ant-btn-primary'])[1]
    Click Web Element    (//button[@class='ant-btn viewBtn___CGYTh']/span[text()='Browse'])[2]
    sleep    2
    Upload File    (//div[@class='uploadBtn___1BT3c'])[2]    ThaiID.jpg
    Click Web Element    (//button[@class='ant-btn ant-btn-primary'])[2]

Verify Personal Addition Infomation
    [Arguments]    ${Personal_Info}
    ${verify_title}    Get Web Value    //div[@id='title']
    ${verify_GivenName}    Get Web Value    //input[@id='givenName']
    ${verify_FamilyName}    Get Web Value    //input[@id='familyName']
    ${verify_birthday}    Get Web Value    //input[@class='ant-calendar-picker-input ant-input ant-input-disabled']
    ${verify_age}    Get Web Value    //input[@id='age']
    Should Be Equal    ${verify_title}    ${Personal_Info['Title']}
    Should Be Equal    ${verify_GivenName}    ${Personal_Info['GivenName']}
    Should Be Equal    ${verify_FamilyName}    ${Personal_Info['FamilyName']}
    Should Be Equal    ${verify_birthday}    ${Personal_Info['DOB']}
    Should Be Equal    ${verify_age}    ${Personal_Info['Age']}

Verify Gendor Match With Title
    Select Listbox From Text    ${PersonalInformation_list_Title}    ${TitleMiss}
    Web Element Should Be Not Visible    ${PersonalInformation_button_Gender_Female_Checked}
    Web Element Should Be Not Visible    ${PersonalInformation_button_Gender_Male_Checked}
    Select Listbox From Text    ${PersonalInformation_list_Title}    ${TitleMr}
    Web Element Should Be Not Visible    ${PersonalInformation_button_Gender_Female_Checked}
    Web Element Should Be Visible    ${PersonalInformation_button_Gender_Male_Checked}

Input Health Questions
    Click Web Element    //form[@id='healthForm']/div[@class='ant-row ant-form-item'][1]//span[text()='No']
    Click Web Element    //form[@id='healthForm']/div[@class='ant-row ant-form-item'][2]//span[text()='No']
    Click Web Element    //form[@id='healthForm']/div[@class='ant-row ant-form-item'][3]//span[text()='Yes']
    Click Web Element    //form[@id='healthForm']//div[@class='question-extra-item'][3]//span[text()='No']
