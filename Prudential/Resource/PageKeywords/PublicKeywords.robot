*** Settings ***
Resource          ../AllKeywords.robot

*** Keywords ***
Login
    [Arguments]    ${Username}    ${Password}
    Select Language    ${Language}
    Input Web Text    ${Login_textbox_username}    ${Username}
    Input Web Text    ${Login_textbox_password}    ${Password}
    Click Web Element    ${Login_button_login}
    Web Element Should Be Visible    ${ePOS_button_newquotation}
    sleep    2

Select Language
    [Arguments]    ${Language}
    ${login_Text}    Get Web Text    ${Login_button_login}
    ${status}    BuiltIn.Run Keyword And Return Status    Should Match Regexp    ${login_Text}    ^[a-zA-Z0-9$@$!%*?&#^-_. +]+$
    Run Keyword If    '${Language}'=='EN' and ${status} != ${True}    Click Web Element    ${Login_img_changelanguage}
    Run Keyword If    '${Language}'=='TH' and ${status} == ${True}    Click Web Element    ${Login_img_changelanguage}
