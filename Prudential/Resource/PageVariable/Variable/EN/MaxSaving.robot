*** Variables ***
${MarriageStatusSingle}    Single
${MarriageStatusMarriage}    Marriage
${MarriageStatusOthers}    Others
${NumberOfChildrenNon}    Non
${NumberOfChildrenOne}    1
${NumberOfChildrenTwo}    2
${NumberOfChildrenMoreThanTwo}    More than 2
${EducationLowerThanBachelorDegree}    Lower than bachelor degree
${EducationBachelorDegree}    Bachelor degree
${EducationHigherThanBachelorDegree}    Higher than bachelor degree
${OccupationBusinessOwner}    Business owner
${OccupationGovernmentOfficer}    Government officer
${OccupationStudent}    Student
${OccupationOthers}    Others
${DoYouNeedThisMoneyForDailyLifeNotNecessary}    Not necessary, this amount was allocated to buy insurance
${DoYouNeedThisMoneyForDailyLifeMayBeNecessary}    May be necessary if cannot have income to cover for this period
${DoYouNeedThisMoneyForDailyLifeMostNecessary}    Most necessary
${TitleMiss}      MISS
${TitleMs}        MS.
${TitleMrs}       MRS.
${TitleMr}        MR.
${Efna}           Efna
${New}            New
${History}        History
${BMIEstimator}    BMI Estimator
