*** Variables ***
${MarriageStatusSingle}    โสด
${MarriageStatusMarriage}    สมรส
${MarriageStatusOthers}    อื่นๆ
${NumberOfChildrenNon}    ไม่มี
${NumberOfChildrenOne}    1
${NumberOfChildrenTwo}    2
${NumberOfChildrenMoreThanTwo}    มากกว่า 2 คน
${EducationLowerThanBachelorDegree}    ต่ำกว่าปริญญาตรี
${EducationBachelorDegree}    ปริญญาตรี
${EducationHigherThanBachelorDegree}    สูงกว่าปริญญาตรี
${OccupationBusinessOwner}    ธุรกิจส่วนตัว
${OccupationGovernmentOfficer}    ข้าราชการ/รัฐวิสาหกิจ/พนักงานบริษัทเอกชน
${OccupationStudent}    นักเรียน/นักศึกษา
${OccupationOthers}    อื่นๆ
${DoYouNeedThisMoneyForDailyLifeNotNecessary}    ไม่จำเป็น เพราะได้แบ่งเงินส่วนนี้ไว้ต่างหากแล้ว
${DoYouNeedThisMoneyForDailyLifeMayBeNecessary}    จำเป็นเล็กน้อย หากไม่สามารถหารายได้มาใช้ในช่วงเวลาดังกล่าว
${DoYouNeedThisMoneyForDailyLifeMostNecessary}    จำเป็นมาก
${TitleMiss}      นางสาว
${TitleMs}        นางสาว
${TitleMrs}       นาง
${TitleMr}        นาย
${Efna}           เครื่องมือช่วยขาย
${New}            ใบเสนอขายใบคำขอเอาประกันภัยใหม่
${History}        ข้อมูลใบเสนอขาย\nข้อมูลใบคำขอเอาประกัน
${BMIEstimator}    เครื่องมือวัดค่าดัชนีมวลกาย
