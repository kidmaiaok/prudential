*** Settings ***
Resource          ../Resource/AllKeywords.robot

*** Test Cases ***
google
    Open Browser    https://www.google.co.th/
    Input Web Text    //input[@name='q']    dpu
    Selenium2Library.Press Key    //input[@name='q']    \\13
    Click Web Element    //div[@id='search']//h3[text()='มหาวิทยาลัยธุรกิจบัณฑิตย์']
    Click Web Element    //div[@id='home-intro']//h5[text()='สมัครเรียนคลิก']
    sleep    3
    Select Window    title=รับสมัครนักศึกษา
    Click Web Element    //img[@src='/upload/content/images/admiss-ad.jpg']
    Click Web Element    //button[text()='สมัครเรียนผ่านระบบออนไลน์']
    Input Web Text    //input[@id='name']    ATMFirstname
    Input Web Text    //input[@id='surname']    ATMLastname
    Input Web Text    //input[@id='mobile']    0123456789
    Input Web Text    //input[@id='email']    test@gmail.com
    Input Web Text    //input[@id='line']    ATM
    Input Web Text    //input[@id='facebook']    facebool.com
    Input Web Text    //input[@id='school']    โรงเรียน เซนต์จอห์น
    Select From Web List By Label    //select[@id='fac']    วิทยาลัยนวัตกรรมด้านเทคโนโลยีและวิศวกรรมศาสตร์
    Select From Web List By Label    //select[@id='course']    วิศวกรรมคอมพิวเตอร์
    Select From Web List By Label    //select[@id='type']    4 ปี
    Select From Web List By Label    //select[@id='section']    ภาคปกติ
    Upload File    //input[@id='portfolio']    Robot Framework.pdf
    ${captcha}    Get Web Value    //input[@id='captcha']
    ${resultCaptcha}    Calculate Captcha    ${captcha}
    Input Web Text    //input[@id='inputText']    ${resultCaptcha}
    sleep    5
    [Teardown]    KillGeckoDriver

A
    log    \ Run Keyword If \"aa\"\'aa\' == "MEA" Run Keywords combine.Verify Db Mp Service Query Online Inv
    ${data_in_current_line}    Set Variable    \ Run Keyword If "aa\\"\\'aa\\' == MEA Run Keywords combine.Verify Db Mp Service Query Online Inv
    Run Keyword If    ${data_in_current_line} != ${EMPTY}    log    pass
    ...    ELSE    log    \fail

*** Keywords ***
Calculate Captcha
    [Arguments]    ${captcha}
    ${captcha}    String.Remove String    ${captcha}    =
    ${captcha}    String.Remove String    ${captcha}    ${SPACE}
    ${captcha}    String.Strip String    ${captcha}
    @{characters}    String.Split String To Characters    ${captcha}
    ${firstnumber}    Set Variable    @{characters}[0]
    ${operator}    Set Variable    @{characters}[1]
    ${lastnumber}    Set Variable    @{characters}[2]
    ${resultCaptcha}    Evaluate    ${firstnumber}${operator}${lastnumber}
    [Return]    ${resultCaptcha}
