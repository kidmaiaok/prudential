*** Settings ***
Resource          ../Resource/AllKeywords.robot

*** Test Cases ***
PRUmaxsaving_A001_TC001
    [Documentation]    "1. Login as UOB.
    ...    2. Select PRUmaxsaving Product.
    ...    3. Fill all mandatory filed at Quotation Page.
    ...    4. Verify Gendor match with Title.
    ...    5. Verify Age calculate by nearest birthday method
    ...    6. Verify occupation relate with age , check list of occupation , Partial search"
    [Setup]    KillGeckoDriver
    Selenium2Library.Open Browser    ${Url}    chrome
    Login    ${Username}    ${Password}
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Verify Gendor Match With Title
    Close All Browsers
    KillGeckoDriver
    [Teardown]    KillGeckoDriver

PRUmaxsaving_A001_TC007
    [Documentation]    1. Verify Button (Back, View, Email,Done,Propersal)
    [Setup]    KillGeckoDriver
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    Click Web Element    ${PersonalInformation_button_Next}
    Input Payor Details
    Click Web Element    ${PayorDetails_button_Next}
    Input Insurance Plan    100000
    Click Web Element    ${InsurancePlan_button_Next}
    Web Element Should Be Visible    ${BenefitIllustration_button_Back}
    Web Element Should Be Visible    ${BenefitIllustration_button_GeneralProvision}
    Web Element Should Be Visible    ${BenefitIllustration_button_View}
    Web Element Should Be Visible    ${BenefitIllustration_button_Email}
    Web Element Should Be Visible    ${BenefitIllustration_button_Done}
    Web Element Should Be Visible    ${BenefitIllustration_button_Proposal}
    Close All Browsers
    [Teardown]    KillGeckoDriver

PRUmaxsaving_Language
    [Documentation]    "1. Show all product list and sequence
    ...    2. Main plan for all premium mode , Gender, Age
    ...    3. Verify Min Max
    ...    4. Rider Premium for all premium mode, Gender, Age"
    [Setup]    KillGeckoDriver
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Web Verify Text    ${Menu_label_Efna}    ${Efna}
    Web Verify Text    ${Menu_label_New}    ${New}
    Web Verify Text    ${Menu_label_History}    ${History}
    Web Verify Text    ${Menu_label_BMIEstimator}    ${BMIEstimator}
    Capture Page Screenshot
    Close All Browsers
    [Teardown]    KillGeckoDriver

PRUmaxsaving_SupportChrome
    [Setup]    KillChromeDriver
    Selenium2Library.Open Browser    ${Url}    chrome
    Login    ${Username}    ${Password}
    Web Verify Text    ${Menu_label_Efna}    ${Efna}
    Web Verify Text    ${Menu_label_New}    ${New}
    Web Verify Text    ${Menu_label_History}    ${History}
    Web Verify Text    ${Menu_label_BMIEstimator}    ${BMIEstimator}
    Close All Browsers
    [Teardown]    KillChromeDriver

PRUmaxsaving_A001_TC001_SupportChrome
    [Setup]    KillChromeDriver
    Selenium2Library.Open Browser    ${Url}    chrome
    Login    ${Username}    ${Password}
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Verify Gendor Match With Title
    Close All Browsers
    KillGeckoDriver
    [Teardown]    KillChromeDriver
