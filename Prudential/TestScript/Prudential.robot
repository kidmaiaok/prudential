*** Settings ***
Test Teardown     SeleniumRedefineKeyword.KillGeckoDriver
Resource          ../Resource/AllKeywords.robot

*** Test Cases ***
POC
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    Click Web Element    ${PersonalInformation_button_Next}
    sleep    3s
    Comment    #PayorDetails
    Comment    Select Listbox From Text    ${PayorDetails_list_Title}    ${TitleMiss}
    Comment    Input Web Text    ${PayorDetails_textbox_GivenName}    Devid
    Comment    Input Web Text    ${PayorDetails_textbox_FamilyName}    Backham
    Comment    Click Web Element    ${PayorDetails_datepicker_BirthDate}
    Comment    # fix birthdate for today
    Comment    Input Web Text    ${PayorDetails_textbox_BirthDate}    14-01-1994
    Comment    Click Web Element    ${PayorDetails_button_Gender_Male}
    Comment    Click Web Element    ${PayorDetails_textbox_Occupation}
    Comment    Click Web Element    //div[@alt='1299-']
    Comment    Input Web Text    ${PayorDetails_textbox_EmailAddress}    test@gmail.com
    Click Web Element    ${PayorDetails_button_Next}
    #Select Main Plan
    Click Web Element    ${InsurancePlan_button_AddMainPlan}
    Click Web Element    ${InsurancePlan_chkbox_PRUMaxSaving}
    Click Web Element    ${InsurancePlan_button_Ok}
    Input Web Text    ${InsurancePlan_textbox_SumAssured}    250000
    Click Web Element    ${InsurancePlan_button_Next}
    Click Web Element    ${InsurancePlan_button_Next}
    Comment    ${BenefitIllustration_button_Email}    #Email
    Comment    ${BenefitIllustration_button_Done}    #Done
    Click Web Element    ${BenefitIllustration_button_Proposal}    #proposal
    Sleep    5s
    #PersonalAddtionalInfo
    Select Listbox From Text    ${PersonalAdditionalInfo_list_Nationality}    Thai
    Select Listbox From Text    ${PersonalAdditionalInfo_list_CountryOfBirth}    Thailand
    Select Listbox From Text    ${PersonalAdditionalInfo_list_IdType}    ID Card
    Input Web Text    ${PersonalAdditionalInfo_textbox_IDNumber}    1103701488701
    #DateExpire
    Click Web Element    ${PersonalAdditionalInfo_datepicker_ExpiryDate}
    Input Web Text    ${PersonalAdditionalInfo_textbox_ExpiryDate}    30-01-2019
    Click Web Element    ${PersonalAdditionalInfo_label_ExpiryDate}
    Select Listbox From Text    ${PersonalAdditionalInfo_list_MaritalStatus}    Single
    Click Web Element    ${PersonalAdditionalInfo_button_ InsuranceForYourself_No}    #No
    Input Web Text    ${PersonalAdditionalInfo_textbox_CurrentAddressNumber}    124
    Run Keywords    Click Web Element    ${PersonalAdditionalInfo_list_CurrentAddressArea}
    ...    AND    Click Web Element    xpath=//li[@title='Bangkok']
    ...    AND    Click Web Element    xpath=//li[@title='Khlong Toei']
    ...    AND    Click Web Element    xpath=//li[@title='Khlong Tan']
    ...    AND    Click Web Element    xpath=//li[@title='10110']
    Input Web Text    ${PersonalAdditionalInfo_textbox_CurrentAddressMobile}    0851169494
    Select Listbox From Text    ${PersonalAdditionalInfo_list_WorkingAddress}    Same as Current Address
    Comment    Select Listbox From Text    ${PersonalAdditionalInfo_list_HouseRegisterAddress}    Same as Current Address
    Select Listbox From Text    ${PersonalAdditionalInfo_list_PolicyMailingAddress}    Current Address
    Comment    Select Listbox From Text    ${PersonalAdditionalInfo_list_MailingAddress}    Current Address
    Input Web Text    ${PersonalAdditionalInfo_textbox_Duty}    Tester
    sleep    10s
    Click Web Element    ${PersonalAdditionalInfo_button_Next}    #Next
    #PayorAddtionalInfo
    Click Web Element    ${PayorAdditionalInfo_button_IsPaySelf}    #Yes
    Click Web Element    ${PayorAdditionalInfo_button_Next}    #Next
    #BeneficiaryInfo
    Click Web Element    ${BeneficiaryInfo_button_AddBeneficiary}
    Select Listbox From Text    ${BeneficiaryInfo_list_Relationship}    Wife
    Select Listbox From Text    ${BeneficiaryInfo_list_Title}    MISS
    Input Web Text    ${BeneficiaryInfo_textbox_GivenName}    Hello
    Input Web Text    ${BeneficiaryInfo_textbox_FamilyName}    World
    Input Web Text    ${BeneficiaryInfo_textbox_Age}    34
    Input Web Text    ${BeneficiaryInfo_textbox_Ratio}    100
    Select Listbox From Text    ${BeneficiaryInfo_list_Address}    Other
    Click Web Element    ${BeneficiaryInfo_button_Ok}
    Click Web Element    ${BeneficiaryInfo_button_Next}
    \    \    \    LifestyleQuestions_button
    \    ${LifestyleQuestions_button_QuestionsTwo_Ever}    Ever
    \    ${LifestyleQuestions_button_QuestionsTwo_Never}
    \    ${LifestyleQuestions_button_QuestionThree_Ever}

Verify Sum Assure
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    Click Web Element    ${PersonalInformation_button_Next}
    Input Payor Details
    Click Web Element    ${PayorDetails_button_Next}
    Comment    Input Insurance Plan
    Web Element Should Be Visible    ${Warning_label_WarningMessage}
    sleep    3s
    Capture Page Screenshot

Verify Age
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    ${age}    Get Web Value    //input[@class='ant-input ant-input-disabled']
    Run Keyword if    ${age} > 17    Fail    Juvenie Must Have Age Less Than 17 Years Old.
    Click Web Element    ${OIC_button_Next}
    sleep    3s
    Capture Page Screenshot

Verify Personal Info
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    ${GivenName}    Get Web Value    ${PersonalInformation_textbox_GivenName}
    ${FamilyName}    Get Web Value    ${PersonalInformation_textbox_FamilyName}
    ${Title}    Get Web Value    ${PersonalInformation_list_Title}
    ${DOB}    Get Web Value    ${PersonalInformation_textbox_DateOfBirth}
    ${Age}    Get Web Value    //input[@id='age']
    ${Personal_Info}    Create Dictionary    GivenName=${GivenName}    FamilyName=${FamilyName}    Title=${Title}    DOB=${DOB}    Age=${Age}
    Click Web Element    ${PersonalInformation_button_Next}
    Input Payor Details
    Click Web Element    ${PayorDetails_button_Next}
    Input Insurance Plan
    Click Web Element    ${InsurancePlan_button_Next}
    Input Benefit Illustration
    Comment    Click Web Element    ${BenefitIllustration_button_Done}
    Verify Personal Addition Infomation    ${Personal_Info}
    Capture Page Screenshot

Verify Disable Field
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    Click Web Element    ${PersonalInformation_button_Next}
    Input Payor Details
    Click Web Element    ${PayorDetails_button_Next}
    Input Insurance Plan
    Click Web Element    ${InsurancePlan_button_Next}
    Input Benefit Illustration
    Comment    Click Web Element    ${BenefitIllustration_button_Done}
    Web Element Should Be Visible    //div[@id='titleInEnglish'][@class='ant-select ant-select-disabled']
    Web Element Should Be Visible    //input[@id='givenNameInEnglish'][@class='ant-input ant-input-disabled']
    Web Element Should Be Visible    //input[@id='familyNameInEnglish'][@class='ant-input ant-input-disabled']
    Comment    Input Personal Additional Info
    Capture Page Screenshot

*** Keywords ***
