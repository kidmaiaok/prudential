*** Settings ***
Resource          ../Resource/AllKeywords.robot

*** Test Cases ***
Maxsaving
    [Setup]    KillGeckoDriver
    Selenium2Library.Open Browser    ${Url}
    Login    ${Username}    ${Password}
    Capture Page Screenshot
    Click Web Element    ${ePOS_button_newquotation}
    Click Web Element    ${ePOS_button_ok}
    Input Insurance Form
    Click Web Element    ${OIC_button_Next}
    Input Personal Information
    Click Web Element    ${PersonalInformation_button_Next}
    Input Payor Details
    Click Web Element    ${PayorDetails_button_Next}
    Input Insurance Plan    100000
    Click Web Element    ${InsurancePlan_button_Next}
    Input Benefit Illustration
    Input Personal Additional Info
    Click Web Element    ${PersonalAdditionalInfo_button_Next}
    Input Payor Additional Info
    Click Web Element    ${PayorAdditionalInfo_button_Next}    #Next
    Input Beneficiary Additional Info
    Click Web Element    ${BeneficiaryInfo_button_Next}
    Input Lifestyle Questions
    Click Web Element    ${LifestyleQuestions_button_Next}
    Input Health Questions
    Click Web Element    ${FATCA_button_Next}
    Input FATCA
    Click Web Element    ${FATCA_button_Next}
    Input KYC/CDD Question
    Click Web Element    ${CDDQuestion_button_Next}
    Input Submission UOB Information
    Click Web Element    ${UOBInformation_button_Next}
    Input Submission Payment
    Click Web Element    ${Payment_button_Next}
    Click Web Element    ${InfoReview_button_Ok}
    Click Web Element    ${InfoReview_button_Ok}
    Input Submission Documents
    Capture Page Screenshot
